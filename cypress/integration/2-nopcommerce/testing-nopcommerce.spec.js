describe('Shopping Cart Test Quantity Update', function(){
    it('Verify the total amount after the quantity updated', function(){
        //1: Launch Browser & Open Url
        cy.visit("https://demo.nopcommerce.com/")

        //2: Enter Text in Search box
        cy.get("#small-searchterms").type("HP Spectre XT Pro UltraBook")

        //3: Click on Search button
        cy.get(".search-box-button[type=submit]").click()
        cy.wait('50000')

        //4: Click on ADD TO CART
        cy.get(".product-box-add-to-cart-button[value='Add to cart']").click()

        //5: Click on Shopping Cart Link at the top of the page
        cy.get("#topcartlink > a > span.cart-label").click()

        //6: Verify the unit price $1,350.00
        //cy.get(".product-unit-price").contains("$1,350.00")
        cy.get(".product-unit-price").should('contain', '$1,350.00')

        //7: Enter Quantity 2
        cy.get(".qty-input").clear().type('2')

        //8: Update shopping cart
        cy.get(".update-cart-button[name=updatecart]").click()

        //9: Verify Total  $2,700.00
        //cy.get(".value-summary > strong").contains('$2,700.00')
        cy.get(".value-summary > strong")
          .should(($total)=>{
              expect($total).to.contain('$2,700.00')
          })
    })
})
