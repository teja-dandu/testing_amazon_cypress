describe('go the url page', () => {
    beforeEach(() => {
        cy.visit('https://www.reddit.com/login/');
    });
    it('loading the alert window', () => {
        cy.get('#loginUsername').click().type('test123');
        cy.get('#loginPassword').click().type('test123');
        cy.get('button[type = submit]').contains(' Log In').click();
    });
});
