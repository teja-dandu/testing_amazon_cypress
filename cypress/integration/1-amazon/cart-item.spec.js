describe('Go to amazon site', () => {
    beforeEach(() => {
        cy.visit('https://www.amazon.com/');
    });
    it('successfully load the site', () => {
        //searching for in the search bar
        cy.get('#nav-search-bar-form').click().type('Introduction to algorithms');
        // click enter on that nav submit button
        cy.get('#nav-search-submit-button').type('{enter}');
        cy.get('[data-asin = 026204630X]').eq(0).contains('Introduction to Algorithms, fourth edition').click();
        cy.get('#add-to-cart-button').click();
    });
})
