describe('Login Form of amazon', () => {
    // const username = "teja.dandu484@gmail.com"
    beforeEach(() => {
        cy.visit('https://www.amazon.in/');
    });
    it('successfully load the site', () => {
        cy.get('#nav-link-accountList').trigger('mouseover');
        cy.get('#nav-flyout-ya-newCust > a').click();

        cy.get('#ap_customer_name').click().type('Dandu Teja');
        cy.get('#ap_email').click().type('tejadandu66@gmail.com');
        cy.get('#ap_password').click().type('test123');
        cy.get('#ap_password_check').click().type('test123');
        cy.get('#continue').click();
    })
})
