describe('Go to amazon site', () => {
    beforeEach(() => {
        cy.visit('https://www.amazon.com/');
    });
    it('login to amazon account', () => {
        cy.get('#nav-link-accountList').trigger('mouseover');
        cy.get('#nav-link-accountList-nav-line-1').click();
        cy.get('#ap_email').click().type('teja.dandu484@gmail.com');
        cy.get('#continue').click();
        cy.get('#ap_password').click().type('Teja@1364');
        cy.get('#signInSubmit').click();
    });
    it('successfully load the site', () => {
        //searching for in the search bar
        cy.get('#nav-search-bar-form').click().type('Introduction to algorithms');
        // click enter on that nav submit button
        cy.get('#nav-search-submit-button').type('{enter}');
        cy.get('[data-asin = 026204630X]').eq(0).contains('Introduction to Algorithms, fourth edition').click();
        cy.get('#buy-now-button').click();
    });
})
